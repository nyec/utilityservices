
import com.sun.javafx.scene.layout.region.Margins;
import sun.rmi.runtime.Log;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Created by dpal on 6/16/2016.
 */
@WebService(targetNamespace = "http://test")
public class PatientLookupService implements IPatientLookUp{

  public static void main(String[] argv) {
    Object implementor = new PatientLookupService ();
    String address = "http://localhost:8080/PatientLookUp/services/PatientLookupService";
    Endpoint.publish(address, implementor);
  }


  @Override
  public PatientDetails GetPatientDetails(String qeMPI, String qeSourceCode) {
    PatientDetails pd = new PatientDetails();
    CallableStatement callableStatement = null;
    String getDBUSERByUserIdSql = "{SELECT uspGetPatientDetails(?,?)}";
    FileHandler fh;
    Logger logger = Logger.getLogger("MyLog");
    try {

      fh = new FileHandler("C:\\Temp\\log\\MyLogFile.log");
      logger.addHandler(fh);
      SimpleFormatter formatter = new SimpleFormatter();
      fh.setFormatter(formatter);
      // the following statement is used to log any messages
      logger.info("My first log");

      String url = "jdbc:sqlserver://192.168.180.45:3978;databaseName=INI_StateWide;user=dpal;password=!Raghav098";
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
      Connection conn = DriverManager.getConnection(url);
      Statement stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery("SELECT * FROM [dbo].[ufn_GetPatientDetails] ("+qeMPI+",'"+qeSourceCode+"')");

      while (rs.next()) {
        pd.setCity(rs.getString("city"));
        pd.setCountry(rs.getString("country"));
        pd.setFirstName(rs.getString("onmfirst"));
        pd.setLastName(rs.getString("onmlast"));
        pd.setQEMPI(rs.getString("memidnum"));
        pd.setsMPI(rs.getString("curentrecno"));
        pd.setState(rs.getString("state"));
        pd.setStreetLine1(rs.getString("stline1"));
        pd.setStreetLine2(rs.getString("stline2"));
        pd.setStreetLine3(rs.getString("stline3"));
        pd.setStreetLine4(rs.getString("stline4"));
        pd.setSSN(rs.getString("SS_number"));
        pd.setDob(rs.getString("dateval"));
        System.out.println(rs.getString("dateval"));
        pd.setPrimaryPhone(rs.getString("primaryphone"));
        pd.setSecondaryPhone(rs.getString("secondaryphone"));
        pd.setZipCode(rs.getString("zipcode"));
        pd.setOtherPhone(rs.getString("otherphone"));
      }
      conn.close();
    } catch (Exception e) {
      System.err.println("Got an exception! ");
      System.err.println(e.getMessage());
      logger.info(e.getMessage());

    }
    return pd;
  }
}
