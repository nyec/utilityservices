import javax.jws.WebMethod;

/**
 * Created by dpal on 6/16/2016.
 */
public interface IPatientLookUp {
    @WebMethod
   PatientDetails GetPatientDetails(String qeMPI,String qeSourceCode);

}
